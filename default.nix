{ config, pkgs, lib, ... }: with lib;

{
  imports = [
    <nixpkgs/nixos/modules/profiles/headless.nix>
  ];

  # Activate NixOS container hooks
  boot.isContainer = true;
  boot.postBootCommands =
    ''
      # After booting, register the contents of the Nix store in the Nix
      # database.
      if [ -f /nix-path-registration ]; then
        ${config.nix.package.out}/bin/nix-store --load-db < /nix-path-registration &&
        rm /nix-path-registration
      fi

      # nixos-rebuild also requires a "system" profile
      ${config.nix.package.out}/bin/nix-env -p /nix/var/nix/profiles/system --set /run/current-system
    '';

  # Disable some features that are not useful in a container.
  services.udisks2.enable = mkDefault false;
  security.polkit.enable = mkDefault false;
  powerManagement.enable = mkDefault false;
  documentation.enable = mkDefault false;
  documentation.man.enable = mkDefault false;
  documentation.info.enable = mkDefault false;
  documentation.nixos.enable = mkDefault false;
  environment.noXlibs = mkDefault true;

  i18n.defaultLocale = mkDefault "en_US.UTF-8";
  i18n.supportedLocales = mkDefault [ "${config.i18n.defaultLocale}/UTF-8" ];

  # Use systemd as initialization script
  system.activationScripts.installInitScript =
    ''ln -fs $systemConfig/init /sbin/init'';

  # Make special mounts quiet by default
  system.activationScripts.specialfs = mkForce
      ''
        specialMount() {
          local device="$1"
          local mountPoint="$2"
          local options="$3"
          local fsType="$4"
          if mountpoint -q "$mountPoint"; then
            local options="remount,$options"
          else
            mkdir -m 0755 -p "$mountPoint"
          fi
          mount -t "$fsType" -o "$options" "$device" "$mountPoint" 2>/dev/null || true
        }
        source ${config.system.build.earlyMountScript}
      '';

  # Disable systemd services that may not run in containers
  systemd.sockets.systemd-journald-audit.enable = false;
  systemd.sockets.systemd-udevd-kernel.enable = false;
  systemd.services.getty.enable = false;
  systemd.services.kmod-static-nodes.enable = false;
  systemd.paths.systemd-ask-password-wall.enable = false;

}
